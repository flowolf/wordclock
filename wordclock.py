#import time
from effects import *
from settings import *
from machine import Pin, unique_id
from math import ceil
#IS_UPY=False

# enable RTC module (DS1307)
RTC = True
#RTC = False
if RTC:
    #https://github.com/adafruit/Adafruit-uRTC
    import urtc
    from machine import I2C

    SCL = 5 # Pin1 ESP8266/ Pin 
    SDA = 4 # Pin2 ESP8266/

    i2c = I2C(sda=Pin(SDA),scl=Pin(SCL))
    rtc = urtc.DS1307(i2c)
    rtc.datetime()

import gc
import micropython as upy
gc.collect()
print("free ram")
print(gc.mem_free())

# micropython specifics
from umqtt.robust import MQTTClient
import network
import neopixel
import ntptime
import utime
import ubinascii

""" word_matrix = "ESKISTLFÜNF
                   ZEHNZWANZIG
                   DREIVIERTEL
                   TNACHGVORJM
                   HALBQZWÖLFP
                   ZWEINSIEBEN
                   XDREIRHFÜNF
                   ELFNEUNVIER
                   WACHTZEHNRS
                   BSECHSÄYUHR"
"""
words = {   "es":0,
            "ist":3,
            "fünf_pre":7,
            "zehn_pre":11,
            "zwanzig":15,
            "dreiviertel":22,
            "viertel":26,
            "nach":34,
            "vor":39,
            "halb":44,
            "zwölf":49,
            "zwei":55,
            "ein":57,
            "eins":57,
            "sieben":60,
            "drei":67,
            "fünf":73,
            "elf":77,
            "neun":80,
            "vier":84,
            "acht":89,
            "zehn":93,
            "sechs":100,
            "uhr":107,
        }

GLOBAL_DIMM = DIMM
BRIGHTNESS = 255
DIMM = GLOBAL_DIMM*(BRIGHTNESS//255)
DEFAULT_COLOR = (int(255*DIMM),int(255*DIMM),int(255*DIMM))
MQTT_COLOR = [255,255,255]
# RTC = None
# RTC = machine.RTC()

# WLAN = ""
# WLAN_PASS = ""
NETWORK_CONNECT_TIMEOUT = 60 #seconds

DEV_ID = "wordclock-{0}".format(ubinascii.hexlify(unique_id()).decode('utf-8'))
print(DEV_ID)

CURR_COMMAND = "clock"
#CURR_COMMAND = "rgb_sparcle"
#CURR_COMMAND = "blue_sparcle"
FORCE_UPDATE = False
CORRECT_TIME = False

if MQTT:
    MQTT_TOPIC = "/" + DEV_ID + MQTT_TOPIC
    MQTT_COMMAND_TOPIC = MQTT_TOPIC + "/command"
    MQTT_COLOR_TOPIC = MQTT_TOPIC + "/color"
    MQTT_BRIGHTNESS_TOPIC = MQTT_TOPIC + "/brightness"
    c = MQTTClient(DEV_ID, MQTT_BROKER, user=MQTT_USER, password=MQTT_PASS)
    print(MQTT_USER)
    #print(MQTT_PASS)
    print(MQTT_COMMAND_TOPIC)

def restore_settings():
    pass

def store_settings():
    pass

def set_time():
    global CORRECT_TIME
    # global RTC
    # year, month, day, hour, minute, second, weekday, yearday
    success = False
    max_tries = 3
    tries = 0
    sleep = 5000
    while not success:
        try:
            ntptime.settime()
        except OSError:
            utime.sleep_ms(sleep)
            if tries < max_tries:
                tries += 1
                utime.sleep_ms(sleep*tries)
                continue
            else:
                if not RTC:
                    CORRECT_TIME = False
                break
        else:
            if RTC:
                # set RTC
                year, month, day, hour, minute, second, weekday, yearday = utime.localtime()
                dtt = urtc.datetime_tuple(year=year,
                                          month=month,
                                          day=day,
                                          hour=hour,
                                          minute=minute,
                                          second=second,
                                          weekday=weekday)
                rtc.datetime(dtt)
            success = True
            CORRECT_TIME = True
    #RTC.datetime((year,month,day,hour,minute,second,0,0))
    if success:
        print("set time to: {}".format(utime.localtime()))
    else:
        print("no time could be set!")

def get_time():
    if RTC:
        year, month, day, weekday, hour, minute, second, millisecond = rtc.datetime()
        yearday = None
        return (year, month, day, hour, minute, second, weekday, yearday)
    else:
        return utime.localtime()

def connect_network():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(WLAN,WLAN_PASS)

class Display():
    def __init__(self,x=11,y=10,z=4):
        # wiring is done in lines à 10 leds
        # there are 11 lines
        # every line corresponds to one x, every led on those lines is
        # one y dimension
        # z are additional leds at the end to indicate minutes
        self.x_dimension = x # to the right
        self.y_dimension = y # down
        self.matrix = [None]*self.x_dimension*self.y_dimension
        self.np = neopixel.NeoPixel(Pin(LEDPIN),x*y+z)
        self.np.fill((0,0,0))
        self.np.write()

    def set(self,x,y,val):
        # print("x: {}, y: {}".format(x,y))
        if x >= self.x_dimension or y >= self.y_dimension:
            raise ValueError
        # flip every second row
        if x%2:
            y = self.y_dimension-y-1
        self.np[x*self.y_dimension+y] = val
#        self.np.write()

    def clear_screen(self):
        self.np.fill((0,0,0))
        self.np.write()

    def set_minutes(self,minutes,val=DEFAULT_COLOR):
        """set minutes indicator"""
        if minutes > 4 or minutes < 0:
            raise ValueError
        if minutes == 0:
            for i in range(0,4):
                self.np[self.x_dimension*self.y_dimension+i] = (0,0,0)
        else:
            for i in range(0,minutes):
                self.np[self.x_dimension*self.y_dimension-1+(4-i)] = val

    def set_word(self,z,length,val=DEFAULT_COLOR):
        """gets z coordinate (like in array) and length,
           optional val for color"""
        for i in range(0,length):
            self.set(z%self.x_dimension+i,z//self.x_dimension,val)

    def show_time_str(self,string,minutes=0,val=DEFAULT_COLOR):
        self.np.fill((0,0,0))
        string = string.lower()
        string = string.strip()
        str_words = string.split()
        if "es ist zehn vor" in string or\
           "es ist fünf vor" in string or\
           "es ist zehn nach" in string or\
           "es ist fünf nach" in string:
            # first zehn or fünf is pre
            str_words[2] += "_pre"

        # if time could not be set correctly, hide "es ist" from time string
        if not CORRECT_TIME:
            str_words.pop(0)
            str_words.pop(0)

        for word in str_words:
            if "_pre" in word:
                length = len(word) - 4
            else:
                length = len(word)
            self.set_word(words[word],length,val)
        self.set_minutes(minutes,val)
        self.np.write()

    def show_time(self,h,m,val=DEFAULT_COLOR):
        """show time on matrix, expects int for h and m"""
        full = False
        string = "es ist "
        h = h%12
        # minute
        if m >= 5 and m < 10:
            string += "fünf nach "
        if m >= 10 and m < 15:
            string += "zehn nach "
        if m >= 15 and m < 20:
            string += "viertel "
            h = (h+1)%12
        if m >= 20 and m < 25:
            string += "zwanzig nach "
        if m >= 25 and m < 30:
            string += "fünf vor halb "
            h = (h+1)%12
        if m >= 30 and m < 35:
            string += "halb "
            h = (h+1)%12
        if m >= 35 and m < 40:
            string += "fünf nach halb "
            h = (h+1)%12
        if m >= 40 and m < 45:
            string += "zwanzig vor "
            h = (h+1)%12
        if m >= 45 and m < 50:
            string += "dreiviertel "
            h = (h+1)%12
        if m >= 50 and m < 55:
            string += "zehn vor "
            h = (h+1)%12
        if m >= 55 and m <= 59:
            string += "fünf vor "
            h = (h+1)%12
        if m >= 0 and m < 5:
            full = True

        hourstrings = [ "zwölf",
                        "ein",
                        "zwei",
                        "drei",
                        "vier",
                        "fünf",
                        "sechs",
                        "sieben",
                        "acht",
                        "neun",
                        "zehn",
                        "elf",
                        "zwölf"]
        string += hourstrings[h%12]
        if h == 1 and not full:
                string += "s"
        if full:
            string += " uhr"
        self.show_time_str(string,m%5,val)

def timezone_and_summertime_offset(t):
    TZ_OFFSET = 1 # CET
    DST_OFFSET = 1 # offset for Daylight saving time
    year, month, day, hour, minute, second, weekday, yearday = t
	#if (_tempDateTime.month < 3 || _tempDateTime.month > 10) return false; // keine Sommerzeit in Jan, Feb, Nov, Dez
	#if (_tempDateTime.month > 3 && _tempDateTime.month < 10) return true; // Sommerzeit in Apr, Mai, Jun, Jul, Aug, Sep
	#if (_tempDateTime.month == 3 && (_tempDateTime.hour + 24 * _tempDateTime.day) >= (3 +  24 * (31 - (5 * _tempDateTime.year / 4 + 4) % 7))
    #   || _tempDateTime.month == 10 && (_tempDateTime.hour + 24 * _tempDateTime.day) < (3 +  24 * (31 - (5 * _tempDateTime.year / 4 + 1) % 7)))
    #return true; +1
    if month < 3 or month > 10: # for sure winter time
        # just set timezone
        hour += TZ_OFFSET
    elif month > 3 and month < 10: # for sure summer time
        hour += TZ_OFFSET + DST_OFFSET

    else:
        # fix in the year 2099!
        switchday = (1 + 24 * ceil(31 - (5 * year / 4 + 4 ) % 7 ) )
        if (month == 3 and (hour + 24 * day) >= switchday ) or \
             (month == 10 and (hour + 24 * day) < switchday )
            hour += TZ_OFFSET + DST_OFFSET
        else:
            hour += TZ_OFFSET

    return (year, month, day, hour, minute, second, weekday, yearday)

def set_command(topic, msg):
    global CURR_COMMAND
    global DEFAULT_COLOR
    global BRIGHTNESS
    global DIMM
    global FORCE_UPDATE
    global MQTT_COLOR
    topic = topic.decode("utf-8")
    msg = msg.decode("utf-8")
    if topic == MQTT_COMMAND_TOPIC:
        CURR_COMMAND = msg
        FORCE_UPDATE = True
    if topic == MQTT_COLOR_TOPIC:
        color = msg
        #try:
        #print(color)
        if "#" in color:
            color = color.split("#")[1]
        if "," in color:
            color = color.split(",")
        else:
            # expect format: FF0033
            color = [int(color[i:i+2],16) for i in range(0,6,2)]
        MQTT_COLOR = [int(i) for i in color]
        color = [int(int(i)*DIMM) for i in color]
        DEFAULT_COLOR = (color[0],color[1],color[2])
        FORCE_UPDATE = True
        print("got color: {}".format(DEFAULT_COLOR))
        # except:
        #     pass
    if topic == MQTT_BRIGHTNESS_TOPIC:
        BRIGHTNESS = int(msg)
        DIMM = GLOBAL_DIMM*(BRIGHTNESS/255)
        DEFAULT_COLOR = [int(i*DIMM) for i in MQTT_COLOR]
        print("DIMM: {}".format(DIMM))
        FORCE_UPDATE = True
        print("got BRIGHTNESS: {}".format(BRIGHTNESS))

def connect_mqtt():
    global c
    if MQTT:
        c.set_callback(set_command)
        c.connect()
        c.subscribe(MQTT_COMMAND_TOPIC)
        c.subscribe(MQTT_COLOR_TOPIC)
        c.subscribe(MQTT_BRIGHTNESS_TOPIC)

# def init_connect_network():
#     print("getting network")
#     network_connect_start_time = time.ticks_ms()
#     import network
#
#     if not wlan.active():
#         wlan.active(True)
#     nets = wlan.scan()
#     for net in nets:
#         net = net[0].decode('utf-8')
#         if net in MY_NETS and not wlan.isconnected():
#             # set local wifi
#             WLAN = net
#             WLAN_PASS = MY_NETS[WLAN]
#             connect_network()
#
# def connect_network():
#     network_connect_start_time = time.ticks_ms()
#     print('connecting to network: {}'.format(net))
#     wlan.connect(WLAN, WLAN_PASS, timeout=5000)
#     while not wlan.isconnected():
#         # if debug:
#         #     led.value(1) # off
#         if wlan.status() == network.STAT_IDLE:
#             print("ERROR: nothing going on")
#             break
#         # if wlan.status() == network.STAT_CONNECTING:
#         #     print("INFO: connecting to network")
#         if wlan.status() == network.STAT_GOT_IP:
#             print("INFO: what are you doing here, shouldn't be here...")
#         if wlan.status() == network.STAT_WRONG_PASSWORD or\
#             wlan.status() == network.STAT_NO_AP_FOUND or\
#             wlan.status() == network.STAT_CONNECT_FAIL:
#
#             print("ERROR: wifi has issues ({})".format(wlan.status()))
#             break
#         if (network_connect_start_time + NETWORK_CONNECT_TIMEOUT*1000) < time.ticks_ms():
#             print("ERROR: network timeout. trying other network, or sleeping")
#             break
#         #machine.idle()
#     # if debug:
#     #     led.value(0) # on
#     # print('network config:', wlan.ifconfig())

# generator
def show_clock(display):
    global FORCE_UPDATE
    d = display
    m = None
    h = None
    while True:
        #print("show clock loop")
        #year, month, day, hour, minute, second, weekday, yearday = get_time()
        year, month, day, hour, minute, second, weekday, yearday = timezone_and_summertime_offset(get_time())
        #d.show_time(hour,minute,val=COLORS[randrange(0,len(COLORS))])
        # stupid simple and dirty timezone fix for CET
        hour = hour % 12
        #print(FORCE_UPDATE)
        if minute != m or FORCE_UPDATE:
            m = minute
            #d.show_time(hour,minute,col)
            d.show_time(hour,minute,DEFAULT_COLOR)
            #d.show_time(hour,minute)
            FORCE_UPDATE = False
            print("{} {} {}, {}:{}:{}".format(year,month,day,hour,minute,second))
        # on change of hour
        #if hour != h:
        # every 30 mins
        if not max(0,minute - 5) % 30 and second == 5:
            h = hour
            set_time()
        yield

def main():
    # global c
    d = Display()
    DELAY = 1000
    #global DEFAULT_COLOR
    timegen = show_clock(d)
    curr_gen = timegen
    OLD_COMMAND = ""
    # rgb_sparclegen = rgb_fixed_sparcle(d,numleds=NUMLEDS)
    # blue_sparclegen = blue_sparcle(d,numleds=NUMLEDS)
    # white_sparclegen = white_sparcle(d,numleds=NUMLEDS)
    # color_sparclegen = color_sparcle(d,numleds=NUMLEDS)
    while True:
        if CURR_COMMAND != OLD_COMMAND:
            init_new_command = True
        else:
            init_new_command = False

        if CURR_COMMAND == "clock" or CURR_COMMAND == "ON":
            if init_new_command:
                del curr_gen
                curr_gen = show_clock(d)
                OLD_COMMAND = CURR_COMMAND
            print(".",end="")
            #print(DEFAULT_COLOR)
            #next(timegen)
            next(curr_gen)
            utime.sleep_ms(DELAY)
        if CURR_COMMAND == "rgb_sparcle":
            DIMM = 0.7
            if init_new_command:
                del curr_gen
                curr_gen = rgb_fixed_sparcle(d,numleds=NUMLEDS)
                OLD_COMMAND = CURR_COMMAND
            # print("show rgb effect")
            #next(rgb_sparclegen)
            next(curr_gen)
        if CURR_COMMAND == "blue_sparcle":
            if init_new_command:
                del curr_gen
                curr_gen = blue_sparcle(d,numleds=NUMLEDS)
                OLD_COMMAND = CURR_COMMAND
            #print("show blue sparcle effect")
            # next(blue_sparclegen)
            next(curr_gen)
        # if CURR_COMMAND == "color_sparcle":
        #     if init_new_command:
        #         del curr_gen
        #         curr_gen = color_sparcle(d,numleds=NUMLEDS)
        #         OLD_COMMAND = CURR_COMMAND
        #     #print("show blue sparcle effect")
        #     # next(color_sparclegen)
        #     next(curr_gen)
        # if CURR_COMMAND == "white_sparcle":
        #     if init_new_command:
        #         del curr_gen
        #         curr_gen = white_sparcle(d,numleds=NUMLEDS)
        #         OLD_COMMAND = CURR_COMMAND
        #     #print("show blue sparcle effect")
        #     # next(color_sparclegen)
        #     next(curr_gen)
        # if CURR_COMMAND == "white_sparcle":
        #     #print("show blue sparcle effect")
        #     next(white_sparclegen)
        if MQTT:
            c.check_msg()

        # if utime.ticks_ms() % 30000 < 100:
        #     connect_network()
        # if gc.isenabled():
        #     gc.collect()
        #     print("free ram: ", end="")
        #     print(gc.mem_free())
        #     print(upy.mem_info())

    # blue_sparcle()
    # rgb_fixed_sparcle()
    # white_sparcle()

if __name__ == "__main__":
    utime.sleep(5)
#    wlan = network.WLAN(network.STA_IF)
#    init_connect_network()
    connect_network()

    # gc.collect()
    # print("free ram")
    # print(gc.mem_free())

    set_time()
    connect_mqtt()
    utime.sleep(2)
    main()
