import time
from random import randrange
import neopixel
from display import Display

DIMM = 0.4
GLOBAL_DIMM = DIMM
BRIGHTNESS = 255
DIMM = GLOBAL_DIMM*(BRIGHTNESS//255)
DEFAULT_COLOR = (int(255*DIMM),int(255*DIMM),int(255*DIMM))
#MQTT_COLOR = [255,255,255]
# RTC = None
# RTC = machine.RTC()

CURR_COMMAND = "matrix"
FORCE_UPDATE = False


THRESHOLD = 8

class Matrix:
    def __init__(self):
        # green = (20,200,40)
        # white = (200,200,200)
        # matrix = [20 for _ in range(10)]
        self.colors = []
        for i in range(0,5):
            self.colors.append((0,int((255.0/10*i)*DIMM),0))
        self.colors.append((int(10*DIMM),int(204*DIMM),int(20*DIMM)))
        self.colors.append((int(30*DIMM),int(230*DIMM),int(35*DIMM)))
        self.colors.append((int(90*DIMM),int(255*DIMM),int(90*DIMM)))
    def run(self):
        rain = [20 for _ in range(0,11)]
        while True:
            d.clear()
            for x,y in enumerate(rain):
                if (y == 20):
                    # reset y coordinate randomly
                    if (randrange(0,11) > THRESHOLD):
                        rain[x] = 0
                else:
                    # simple alpha blending using our predefined colors
                    y0 = max(y - len(self.colors)+1, 0)
                    y1 = min(9, y)
                    ci = y0 - (y - len(self.colors)+1)
                    for yi,yn in enumerate(range(y0, y1 + 1)):
                        color = self.colors[ci + yi]
                        #print(color)
                        d.setPixelRGB(x+1,yn+1,color)
                    # advance y coordinate
                    rain[x] = y + 1
            d.show()
            # input handling
            time.sleep(0.12)

d = Display()
def main():
    # global c
    #d = Display()
    d.clear()
    DELAY = 1000
    matrix = Matrix()
    matrix.run()
    # while True:
    #     pass

if __name__ == "__main__":
#    time.sleep(5)
    try:
        main()
    except KeyboardInterrupt:
        d.clear()
        d.clear()
