#!/usr/bin/env python3

from display import *
import time
import arrow # better time lib
import argparse

RGB = (60,0,0)
RGB = (66,66,66)
RGB = (6,55,55)
DEFAULT_COLOR = RGB

class Clock:
    """ word_matrix = "ESKISTLFÜNF
                       ZEHNZWANZIG
                       DREIVIERTEL
                       VORÜBERNACH
                       HALBQZWÖLFP
                       ZWEINSIEBEN
                       XDREIRHFÜNF
                       ELFNEUNVIER
                       WACHTZEHNJM
                       BSECHSÄYUHR"
    """
    def __init__(self,display):
        self.d = display
        self.words = {  "es":0,
                        "ist":3,
                        "fünf_pre":7,
                        "zehn_pre":11,
                        "zwanzig":15,
                        "dreiviertel":22,
                        "viertel":26,
                        "vor":33,
                        "über":36,
                        "nach":40,
                        "halb":44,
                        "zwölf":49,
                        "zwei":55,
                        "ein":57,
                        "eins":57,
                        "sieben":60,
                        "drei":67,
                        "fünf":73,
                        "elf":77,
                        "neun":80,
                        "vier":84,
                        "acht":89,
                        "zehn":93,
                        "sechs":100,
                        "uhr":107,
                    }
        self.buffer = [[(0,0,0) for y in range(0,self.d.HEIGHT)] for x in range(0,self.d.WIDTH)]
        self.buffer_mins = [(0,0,0) for m in range(0,4)]

    def clear_buffer(self):
        self.buffer = [[(0,0,0) for y in range(0,self.d.HEIGHT)] for x in range(0,self.d.WIDTH)]
        self.buffer_mins = [(0,0,0) for m in range(0,4)]

    def clear_buffer_mins(self):
        self.buffer_mins = [(0,0,0) for m in range(0,4)]

    def get_time(self):
        now = arrow.now()
        hour = now.hour
        minute = now.minute
        second = now.second
        return (hour, minute, second)

    def write_words(self):
        """write buffer to display"""
        for x in range(0,self.d.WIDTH):
            for y in range(0,self.d.HEIGHT):
                self.d.setPixelRGB(x+1,y+1,self.buffer[x][y])
        for val, i in zip(self.buffer_mins, self.d.index_of_additionals):
            self.d.setPixelRGBByIndex(i,val)
        self.d.show()

    def set_minutes(self,minutes,val=DEFAULT_COLOR):
        """set minutes indicator"""
        if minutes > 4 or minutes < 0:
            raise ValueError
        if minutes == 0:
            #for i in self.d.index_of_additionals:
                #self.d.setPixelRGBByIndex(i,(0,0,0))
            self.clear_buffer_mins()
        else:
            for i in range(0,minutes):
                #self.d.setPixelRGBByIndex(self.d.index_of_additionals[i],val)
                self.buffer_mins[i] = val

    def set_word(self,z,length,val=DEFAULT_COLOR):
        """gets z coordinate (like in array) and length,
           optional val for color"""
        for i in range(0,length):
            x = z%self.d.WIDTH+i
            y = z//self.d.WIDTH
            self.buffer[x][y] = val
            #self.d.setPixelRGB(z%self.d.WIDTH+i,z//self.d.WIDTH,val)

    def show_time_str(self,string,minutes=0,val=DEFAULT_COLOR):
        self.d.clear(show=False) # clear but don't update

        # clear buffer
        self.clear_buffer()

        string = string.lower()
        string = string.strip()
        str_words = string.split()
        if "es ist zehn vor" in string or\
           "es ist fünf vor" in string or\
           "es ist zehn nach" in string or\
           "es ist fünf nach" in string:
            # first zehn or fünf is pre
            str_words[2] += "_pre"

        for word in str_words:
            if "_pre" in word:
                length = len(word) - 4
            else:
                length = len(word)
            self.set_word(self.words[word],length,val)
        self.set_minutes(minutes,val)
        self.write_words()
        self.d.show()

    def show_time(self,h,m,val=DEFAULT_COLOR):
        """show time on matrix, expects int for h and m"""
        full = False # exactly full hour
        string = "es ist "
        h = h%12
        # minute
        if m >= 5 and m < 10:
            string += "fünf nach "
        if m >= 10 and m < 15:
            string += "zehn nach "
        if m >= 15 and m < 20:
            string += "viertel "
            h = (h+1)%12
        if m >= 20 and m < 25:
            string += "zwanzig nach "
        if m >= 25 and m < 30:
            string += "fünf vor halb "
            h = (h+1)%12
        if m >= 30 and m < 35:
            string += "halb "
            h = (h+1)%12
        if m >= 35 and m < 40:
            string += "fünf nach halb "
            h = (h+1)%12
        if m >= 40 and m < 45:
            string += "zwanzig vor "
            h = (h+1)%12
        if m >= 45 and m < 50:
            string += "dreiviertel "
            h = (h+1)%12
        if m >= 50 and m < 55:
            string += "zehn vor "
            h = (h+1)%12
        if m >= 55 and m <= 59:
            string += "fünf vor "
            h = (h+1)%12
        if m >= 0 and m < 5:
            full = True

        hourstrings = [ "zwölf",
                        "ein",
                        "zwei",
                        "drei",
                        "vier",
                        "fünf",
                        "sechs",
                        "sieben",
                        "acht",
                        "neun",
                        "zehn",
                        "elf",
                        "zwölf"]
        string += hourstrings[h%12]
        if h == 1 and not full:
                string += "s"
        if full:
            string += " uhr"
        self.show_time_str(string,m%5,val)

    # generator
    def show_clock(self):
        m = None
        h = None
        while True:
            #year, month, day, hour, minute, second, weekday, yearday = timezone_and_summertime_offset(get_time())
            hour, minute, second = self.get_time()
            hour = hour % 12
            #d.show_time(hour,minute,col)
            self.show_time(hour,minute,val=DEFAULT_COLOR)
            #d.show_time(hour,minute)
            if minute != m:
                m = minute
                print("{}:{}:{}".format(hour,minute,second))
            yield

def main():
    d = Display()
    DELAY = 1000
    clock = Clock(d)
    for i in range(0,5):
        d.clear()
    timegen = clock.show_clock()
    curr_gen = timegen
    OLD_COMMAND = ""
    CURR_COMMAND = "clock"

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()
    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        time.sleep(1)
        while True:
            if CURR_COMMAND != OLD_COMMAND:
                init_new_command = True
            else:
                init_new_command = False

            if CURR_COMMAND == "clock" or CURR_COMMAND == "ON":
                d.clear(show=False)
                next(curr_gen)
                time.sleep(DELAY/100000)
    except KeyboardInterrupt:
        if args.clear:
            d.clear()

if __name__ == "__main__":
    main()
