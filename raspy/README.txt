# installation:

apt-get update; apt-get -y upgrade


# https://github.com/jgarff/rpi_ws281x
sudo su -
echo 'blacklist snd_bcm2835' >> /etc/modprobe.d/snd-blacklist.conf
sed -i 's/^dtparam=audio=on/#dtparam=audio=on/g' /boot/config.txt

apt-get install -y vim gcc make build-essential python3-dev python-dev git scons swig


exit
cd
git clone https://github.com/jgarff/rpi_ws281x
cd rpi_ws281x
scons
cd python
sudo python3 setup.py build
sudo python3 setup.py install

cd examples
sudo python3 strandtest.py


cat /etc/systemd/system/wordclock.service
[Unit]
Description=Wordclock
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/sudo -u pi screen -d -m -S wordclock sudo /usr/bin/python3 /home/pi/wordclock/clock.py -c
ExecStop=kill -9 `ps aux | grep wordclock| grep python3| cut -d " " -f 7`;$(sleep 5 &&/usr/bin/python3 /home/pi/wordclock/clear.py)
RemainAfterExit=yes

[Install]
WantedBy=default.target
#----
