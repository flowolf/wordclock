#!/usr/bin/env python3
import os, time
from random import randrange
#from settings import DIMM
from display import *
#INTERVAL = 3
#DIM = [10,11]
DIMM = 0.3
DEBUG = True
#no_diff = 0
class GameOfLife:
    def __init__(self,display,dimm=0.5,interval=3):
        self.DIM = [11,10]
        self.DIMM = dimm
        self.d = display
        self.X = self.DIM[0]
        self.Y = self.DIM[1]
        self.interval = interval
        self.non_color = (0,0,0)
        self.color = (50,50,0)#(0,int(255*self.DIMM),int(255*self.DIMM))#(int(255*self.DIMM),int(255*self.DIMM),int(255*self.DIMM))
        self.no_diff = 0
        self.world = [[0 for y in range(self.Y)] for x in range(self.X)]
        self.world_next = [[0 for y in range(self.Y)] for x in range(self.X)]
        self.generation_count = 0

        #self.next_world = [[0 for x in range(DIM[1])] for y in range(DIM[0])]
        self._reinit()
        print("init GOL")
#        time.sleep_ms(3000)

    def _reinit(self):
        self.d.clear(show=False)
        print("re-init GOL")
        for i in range(self.X):
            for j in range(self.Y):
                if randrange(0,10) > 7:
                    #self.world_next[i][j] = randrange(0,2)
                    self.world_next[i][j] = 1
        self.generation_count = 0
        self._write_world()
        self.d.show()

    def __next__(self):
        print("-"*80)
        self._show_world()
        self._play_god()
        self.generation_count += 1
        if self.no_diff > 2 or self.generation_count > 200:
            self._reinit()
            self.no_diff = 0

    def __iter__(self):
        return self

    def _write_world(self):
        self.d.clear(show=False)
        for x in range(self.X):
            for y in range(self.Y):
                if self.world_next[x][y] == 0:
                    self.d.setPixelRGB(x+1,y+1,self.non_color)
                else:
                    self.d.setPixelRGB(x+1,y+1,self.color)
        del self.world
        self.world = self.world_next
        del self.world_next
        self.world_next = [[0 for y in range(self.Y)] for x in range(self.X)]
        self.d.show()

    def _count_neighbors(self,x,y):
        count = 0
        for i in [x-1,x,x+1]:
            for j in [y-1,y,y+1]:
                if i >= 0 and j >= 0 and i < self.X and j < self.Y and \
                   not (x == i and y == j):
                    count += 1 if self.world[i][j] != 0 else 0
        return count

    def _play_god(self):
        # print("play god...")
        for x in range(self.X):
            for y in range(self.Y):
                self.world_next[x][y] = 0 if self.world[x][y] == 0 else 1
        diff = 0
        for j in range(self.Y):
            for i in range(self.X):
                count = self._count_neighbors(i,j)
                if count == 3 and self.world[i][j] == 0:
                    # create
                    self.world_next[i][j] = 1
                    diff += 1
                if count == 2:
                    pass #continue # live on, but don't breed
                elif (count <= 1 or count > 3):# and not self.world[i][j] == 0:
                    # kill
                    self.world_next[i][j] = 0
                    #diff += 1
            #     if DEBUG:
            #         print(count, end=" ")
            # if DEBUG:
            #     print("", end="\n")

        if diff == 0:
            self.no_diff += 1

        if DEBUG:
            print("diff: {}".format(diff))
        # write world
        self._write_world()

    def _show_world(self):
        for y in range(self.Y):
            for x in range(self.X):
                if self.world[x][y] == 1:
                    c = "x"
                else:
                    c = " "
                print(c, end=" ")
            print("")
