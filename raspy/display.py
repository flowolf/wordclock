#!/usr/bin/env python3

import time
from neopixel import *
import argparse
from math import sin, cos
import clock
import arrow

# LED strip configuration:
LED_COUNT      = 114      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 55     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

class Display():
    def __init__(self, height=10, width=11, additional=4):
        self.HEIGHT = height
        self.WIDTH = width
        self.LED_COUNT = self.HEIGHT * self.WIDTH + additional
        self.strip = Adafruit_NeoPixel(self.LED_COUNT, LED_PIN, LED_FREQ_HZ,
                                       LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
        self.strip.begin()
        self.index_of_additionals = [30,51,72,93]

    def getStripIndex(self, x, y):
        """seen from front, index starts on left top and goes down. next column
        it comes up again, and then down, and so on. on column 3,5,7,9 there
        are additional 1 leds. 11 columns in total and 10 rows.
            1 2 3 4 5 6 7 8 9 0 11
        1   0 0-0 0-0 0-0 0-0 0-0
        2   0 0 0 0 0 0 0 0 0 0 0
        3   0 0 0 0 0 0 0 0 0 0 0
        4   0 0 0 0 0 0 0 0 0 0 0
        5   0 0 0 0 0 0 0 0 0 0 0
        6   0 0 0 0 0 0 0 0 0 0 0
        7   0 0 0 0 0 0 0 0 0 0 0
        8   0 0 0 0 0 0 0 0 0 0 0
        9   0 0 0 0 0 0 0 0 0 0 0
        10  0-0 0 0 0 0 0 0 0 0 0
        11      0/  0/  0/  0/
        """

        if x%2 == 0:
            # reverse index
            index = (x)*self.HEIGHT-y
        else:
            index = (y-1)+(x-1)*self.HEIGHT
        if x > 3:
            # offset for additional leds on bottom
            index += (x-2)//2
        return index

    def getNumPixels(self):
        return self.LED_COUNT

    def clear(self, show=True):
        for i in range(self.LED_COUNT):
            self.strip.setPixelColor(i, Color(0,0,0))
        if show:
            self.show()

    def show(self):
        self.strip.show()

    def setPixelRGB(self, x, y, rgb):
        self.strip.setPixelColor(self.getStripIndex(x,y),Color(rgb[1],rgb[0],rgb[2]))

    def setPixelColor(self, x, y, color):
        self.strip.setPixelColor(self.getStripIndex(x,y),color)

    def setPixelRGBByIndex(self, index, rgb):
        self.strip.setPixelColor(index,Color(rgb[1],rgb[0],rgb[2]))

    def setPixelColorByIndex(self, index, color):
        self.strip.setPixelColor(index,color)

def rainbow(display, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for x in range(1,display.WIDTH+1):
            for y in range(1,display.HEIGHT+1):
                display.setPixelColor(x,y, wheel((x+j) & 255))
        display.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(display, direction=0, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across the x axis."""
    if direction == 0:
        direction = -1
    else:
        direction = -1*direction
    for j in range(256*iterations):
        for x in range(1,display.WIDTH+1):
            for y in range(1,display.HEIGHT+1):
                display.setPixelColor(x,y, wheel((int(x * 256 / display.WIDTH) + j*direction) & 255))
        display.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycleDiagonal(display, direction=0, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    if direction == 0:
        direction = -1
    else:
        direction = -1*direction
    for j in range(256*iterations):
        for x in range(1,display.WIDTH+1):
            for y in range(1,display.HEIGHT+1):
                display.setPixelColor(x,y, wheel((int((x-y) * 256/1.4 / display.WIDTH) + j*direction) & 255))
        display.show()
        time.sleep(wait_ms/1000.0)

def rainbowPlasma(display, direction=5, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    if direction == 0:
        direction = -1
    else:
        direction = -1*direction
    for j in range(256*iterations):
        for x in range(1,display.WIDTH+1):
            for y in range(1,display.HEIGHT+1):
                display.setPixelColor(x,y, wheel((int((sin(5*x*sin(j/150)+y*cos(j/100))+j/150) * 256 / display.WIDTH) + j*direction) & 255))
        display.show()
        time.sleep(wait_ms/1000.0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def time_for_secs(display, secs=10):
    """ show the time for specified seconds"""
    c = clock.Clock(display)
    DELAY = 1000
    starttime = arrow.now().timestamp
    for i in range(0,5):
        d.clear()
    timegen = c.show_clock()
    curr_gen = timegen
    while True:
        d.clear(show=False)
        next(curr_gen)
        time.sleep(DELAY/100000)
        if starttime < arrow.now().timestamp-secs:
            d.clear(show=False) # clear minutes
            break

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()
    d = Display()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        while True:
            print ('rainbow animations')
            #rainbowCycleDiagonal(d,direction=-3)
            rainbowCycleDiagonal(d,direction=5)
            time_for_secs(d,20)
            rainbow(d)
            rainbowCycle(d,direction=4)

    except KeyboardInterrupt:
        if args.clear:
            d.clear()
