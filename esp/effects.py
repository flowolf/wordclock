#import machine, neopixel, network
from random import randrange

NUMLEDS = 110+4

import utime
from settings import DIMM, LEDPIN

# generator
def rgb_fixed_sparcle(display,numleds=NUMLEDS):
    """sparcle effect"""
    print("enter rgb sparcle")
    np = display.np
#    np = neopixel.NeoPixel(machine.Pin(LEDPIN),NUMLEDS)
    np.fill((0,0,0))
    np.write()
    colors = [(r,g,b) for r in range(0,256,45) for g in range(0,256,45) for b in range(0,256,45) if not r == g and not g == b and not r == b ]

    for i in range(0,len(colors)):
        colors[i] = (int(colors[i][0]*DIMM),int(colors[i][1]*DIMM),int(colors[i][2]*DIMM))

    for i in range(0,numleds):
#        matrix[i] = colors[randrange(0,len(colors))]
        np[i] = colors[randrange(0,len(colors))]
    np.write()
    while True:
        rand = randrange(0,numleds)
        np[rand] = colors[randrange(0,len(colors))]
        np.write()
        yield

# generator
def blue_sparcle(display,numleds=NUMLEDS):
    """blue sparcle effect"""
    #np = neopixel.NeoPixel(machine.Pin(LEDPIN),NUMLEDS)
    np = display.np
    rrandom = (0,2) # cannot be (0,[01])
    grandom = (0,60)
    brandom = (0,255)
    np.fill((0,0,0))
    np.write()
    # DIMM COLORS
    #DIMM = 0.2
    # for i in range(0,len(colors)):
    #     colors[i] = (randrange(rrandom[0],rrandom[1]),
    #                  randrange(grandom[0],grandom[1]),
    #                  randrange(brandom[0],brandom[1]),
    #                  )

    for i in range(0,NUMLEDS):
        num = randrange(brandom[0],brandom[1])
        np[i] = (0,
                 num//12,
                 num//10,
                )
    np.write()
    while True:
        rand = randrange(0,NUMLEDS)
        num = randrange(brandom[0],brandom[1])
        np[rand] = (0,
                 num//12,
                 num//10,
                )
        np.write()
        yield

# generator
def color_sparcle(display,color,numleds=NUMLEDS):
    """random color sparcle effect with given color"""
    #np = neopixel.NeoPixel(machine.Pin(LEDPIN),NUMLEDS)
    np = display.np
    np.fill((0,0,0))
    np.write()
    for i in range(0,NUMLEDS):
        c = [i for i in color]
        diff = randrange(10,40)
        plusorminus = randrange(0,2)
        if plusorminus:
            # c[0] = min(c[0]+diff,255)
            # c[1] = min(c[1]+diff,255)
            # c[2] = min(c[2]+diff,255)
            c[0] = min(c[0]+diff,color[0])
            c[1] = min(c[1]+diff,color[1])
            c[2] = min(c[2]+diff,color[2])
        else:
            # c[0] = max(c[0]-diff,color[0])
            # c[1] = max(c[1]-diff,color[1])
            # c[2] = max(c[2]-diff,color[2])
            c[0] = max(c[0]-diff,0)
            c[1] = max(c[1]-diff,0)
            c[2] = max(c[2]-diff,0)
        if i % 100 == 0:
            print(c)
        np[i] = (int(c[0]*DIMM),
                 int(c[1]*DIMM),
                 int(c[2]*DIMM))
    np.write()
    while True:
        rand = randrange(0,NUMLEDS)
        diff = randrange(10,40)
        plusorminus = randrange(0,2)
        if plusorminus:
            # c[0] = min(c[0]+diff,255)
            # c[1] = min(c[1]+diff,255)
            # c[2] = min(c[2]+diff,255)
            c[0] = min(c[0]+diff,color[0])
            c[1] = min(c[1]+diff,color[1])
            c[2] = min(c[2]+diff,color[2])
        else:
            # c[0] = max(c[0]-diff,color[0])
            # c[1] = max(c[1]-diff,color[1])
            # c[2] = max(c[2]-diff,color[2])
            c[0] = max(c[0]-diff,0)
            c[1] = max(c[1]-diff,0)
            c[2] = max(c[2]-diff,0)
        np[rand] = (int(c[0]*DIMM),
                    int(c[1]*DIMM),
                    int(c[2]*DIMM))
        np.write()
        yield
