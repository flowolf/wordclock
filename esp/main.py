#import time
from effects import *
from settings import *
from machine import Pin, unique_id
#IS_UPY=False

# enable RTC module (DS1307)
RTC = True
if RTC:
    #https://github.com/adafruit/Adafruit-uRTC
    import urtc
    from machine import I2C

    SCL = 5 # Pin1
    SDA = 4 # Pin2

    i2c = I2C(sda=Pin(SDA),scl=Pin(SCL))
    rtc = urtc.DS1307(i2c)
    rtc.datetime()

import gc
import micropython as upy
gc.collect()
print("free ram")
print(gc.mem_free())

# micropython specifics
from umqtt.robust import MQTTClient
import network
import neopixel
import ntptime
import utime
import ubinascii

""" word_matrix = "ESKISTLFÜNF
                   ZEHNZWANZIG
                   DREIVIERTEL
                   VORÜBERNACH
                   HALBQZWÖLFP
                   ZWEINSIEBEN
                   XDREIRHFÜNF
                   ELFNEUNVIER
                   WACHTZEHNJM
                   BSECHSÄYUHR"
"""
words = {   "es":0,
            "ist":3,
            "fünf_pre":7,
            "zehn_pre":11,
            "zwanzig":15,
            "dreiviertel":22,
            "viertel":26,
            "vor":33,
            "über":36,
            "nach":40,
            "halb":44,
            "zwölf":49,
            "zwei":55,
            "ein":57,
            "eins":57,
            "sieben":60,
            "drei":67,
            "fünf":73,
            "elf":77,
            "neun":80,
            "vier":84,
            "acht":89,
            "zehn":93,
            "sechs":100,
            "uhr":107,
        }

#word_matrix = "ESKISTLFÜNFZEHNZWANZIGDREIVIERTELTNACHGVORJMHALBQZWÖLFPZWEINSIEBENKDREIRHFÜNFELFNEUNVIERWACHTZEHNRSBSECHSFMUHR"
GLOBAL_DIMM = DIMM
BRIGHTNESS = 255
DIMM = GLOBAL_DIMM*(BRIGHTNESS//255)
DEFAULT_COLOR = (int(255*DIMM),int(255*DIMM),int(255*DIMM))
MQTT_COLOR = [255,255,255]
# RTC = None
# RTC = machine.RTC()

# WLAN = ""
# WLAN_PASS = ""
NETWORK_CONNECT_TIMEOUT = 60 #seconds

DEV_ID = "wordclock-{0}".format(ubinascii.hexlify(unique_id()).decode('utf-8'))
print(DEV_ID)

CURR_COMMAND = "clock"
#CURR_COMMAND = "rgb_sparcle"
#CURR_COMMAND = "blue_sparcle"
FORCE_UPDATE = False
FORCE_NEW_COLOR = False
CORRECT_TIME = False
if RTC:
    CORRECT_TIME = True

if MQTT:
    MQTT_TOPIC = "/" + DEV_ID + MQTT_TOPIC
    MQTT_COMMAND_TOPIC = MQTT_TOPIC + "/command"
    MQTT_COLOR_TOPIC = MQTT_TOPIC + "/color"
    MQTT_BRIGHTNESS_TOPIC = MQTT_TOPIC + "/brightness"
    c = MQTTClient(DEV_ID, MQTT_BROKER, user=MQTT_USER, password=MQTT_PASS)
    print(MQTT_USER)
    #print(MQTT_PASS)
    print(MQTT_COMMAND_TOPIC)

def restore_settings():
    pass

def store_settings():
    pass

def set_time():
    SET_RTC = False
    try:
        ntptime.settime()
    except OSError:
        pass
    else:
        SET_RTC = True
    if RTC and SET_RTC:
        year, month, day, hour, minute, second, weekday, yearday = utime.localtime()
        dtt = urtc.datetime_tuple(year=year,
                                  month=month,
                                  day=day,
                                  hour=hour,
                                  minute=minute,
                                  second=second,
                                  weekday=weekday)
        rtc.datetime(dtt)

def get_time():
    if RTC:
        year, month, day, weekday, hour, minute, second, millisecond = rtc.datetime()
        yearday = None
        return (year, month, day, hour, minute, second, weekday, yearday)
    else:
        return utime.localtime()

def connect_network():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(WLAN,WLAN_PASS)
    # turn off AP wifi
    ap = network.WLAN(network.AP_IF)
    ap.active(False)

class Display():
    def __init__(self,x=11,y=10,z=4):
        # wiring is done in lines à 10 leds
        # there are 11 lines
        # every line corresponds to one x, every led on those lines is
        # one y dimension
        # z are additional leds at the end to indicate minutes
        self.x_dimension = x # to the right
        self.y_dimension = y # down
        self.matrix = [None]*self.x_dimension*self.y_dimension
        self.np = neopixel.NeoPixel(Pin(LEDPIN),x*y+z)
        self.np.fill((0,0,0))
        self.np.write()

    def set(self,x,y,val):
        # print("x: {}, y: {}".format(x,y))
        if x >= self.x_dimension or y >= self.y_dimension:
            raise ValueError
        # flip every second row
        if x%2:
            y = self.y_dimension-y-1
        self.np[x*self.y_dimension+y] = val
#        self.np.write()

    def clear_screen(self):
        self.np.fill((0,0,0))
        self.np.write()

    def set_minutes(self,minutes,val=DEFAULT_COLOR):
        """set minutes indicator"""
        if minutes > 4 or minutes < 0:
            raise ValueError
        if minutes == 0:
            for i in range(0,4):
                self.np[self.x_dimension*self.y_dimension+i] = (0,0,0)
        else:
            for i in range(0,minutes):
                self.np[self.x_dimension*self.y_dimension-1+(4-i)] = val

    def set_word(self,z,length,val=DEFAULT_COLOR):
        """gets z coordinate (like in array) and length,
           optional val for color"""
        for i in range(0,length):
            self.set(z%self.x_dimension+i,z//self.x_dimension,val)

    def show_time_str(self,string,minutes=0,val=DEFAULT_COLOR):
        self.np.fill((0,0,0))
        string = string.lower()
        string = string.strip()
        str_words = string.split()
        if "es ist zehn vor" in string or\
           "es ist fünf vor" in string or\
           "es ist zehn nach" in string or\
           "es ist fünf nach" in string:
            # first zehn or fünf is pre
            str_words[2] += "_pre"

        # if time could not be set correctly, hide "es ist" from time string
        if not CORRECT_TIME:
            str_words.pop(0)
            str_words.pop(0)

        for word in str_words:
            if "_pre" in word:
                length = len(word) - 4
            else:
                length = len(word)
            self.set_word(words[word],length,val)
        self.set_minutes(minutes,val)
        self.np.write()

    def show_time(self,h,m,val=DEFAULT_COLOR):
        """show time on matrix, expects int for h and m"""
        full = False
        string = "es ist "
        h = h%12
        # minute
        if m >= 5 and m < 10:
            string += "fünf nach "
        if m >= 10 and m < 15:
            string += "zehn nach "
        if m >= 15 and m < 20:
            string += "viertel "
            h = (h+1)%12
        if m >= 20 and m < 25:
            string += "zwanzig nach "
        if m >= 25 and m < 30:
            string += "fünf vor halb "
            h = (h+1)%12
        if m >= 30 and m < 35:
            string += "halb "
            h = (h+1)%12
        if m >= 35 and m < 40:
            string += "fünf nach halb "
            h = (h+1)%12
        if m >= 40 and m < 45:
            string += "zwanzig vor "
            h = (h+1)%12
        if m >= 45 and m < 50:
            string += "dreiviertel "
            h = (h+1)%12
        if m >= 50 and m < 55:
            string += "zehn vor "
            h = (h+1)%12
        if m >= 55 and m <= 59:
            string += "fünf vor "
            h = (h+1)%12
        if m >= 0 and m < 5:
            full = True

        hourstrings = [ "zwölf",
                        "ein",
                        "zwei",
                        "drei",
                        "vier",
                        "fünf",
                        "sechs",
                        "sieben",
                        "acht",
                        "neun",
                        "zehn",
                        "elf",
                        "zwölf"]
        string += hourstrings[h%12]
        if h == 1 and not full:
                string += "s"
        if full:
            string += " uhr"
        self.show_time_str(string,m%5,val)

def timezone_offset():
    offset = 1 # CET
    # offset = 2 # CEST
    return offset

def set_command(topic, msg):
    global CURR_COMMAND
    global DEFAULT_COLOR
    global BRIGHTNESS
    global DIMM
    global FORCE_UPDATE
    global MQTT_COLOR
    global FORCE_NEW_COLOR
    topic = topic.decode("utf-8")
    msg = msg.decode("utf-8")
    if topic == MQTT_COMMAND_TOPIC:
        CURR_COMMAND = msg
        FORCE_UPDATE = True
    if topic == MQTT_COLOR_TOPIC:
        color = msg
        #try:
        #print(color)
        if "#" in color:
            color = color.split("#")[1]
        if "," in color:
            color = color.split(",")
        else:
            # expect format: FF0033
            color = [int(color[i:i+2],16) for i in range(0,6,2)]
        MQTT_COLOR = [int(i) for i in color]
        color = [int(int(i)*DIMM) for i in color]
        DEFAULT_COLOR = (color[0],color[1],color[2])
        FORCE_UPDATE = True
        FORCE_NEW_COLOR = True
        print("got color: {}".format(DEFAULT_COLOR))
        # except:
        #     pass
    if topic == MQTT_BRIGHTNESS_TOPIC:
        BRIGHTNESS = int(msg)
        DIMM = GLOBAL_DIMM*(BRIGHTNESS/255)
        DEFAULT_COLOR = [int(i*DIMM) for i in MQTT_COLOR]
        print("DIMM: {}".format(DIMM))
        FORCE_UPDATE = True
        print("got BRIGHTNESS: {}".format(BRIGHTNESS))

def connect_mqtt():
    global c
    if MQTT:
        c.set_callback(set_command)
        c.connect()
        c.subscribe(MQTT_COMMAND_TOPIC)
        c.subscribe(MQTT_COLOR_TOPIC)
        c.subscribe(MQTT_BRIGHTNESS_TOPIC)

# generator
def show_clock(display):
    global FORCE_UPDATE
    d = display
    m = None
    h = None
    while True:
        #print("show clock loop")
        year, month, day, hour, minute, second, weekday, yearday = get_time()
        #d.show_time(hour,minute,val=COLORS[randrange(0,len(COLORS))])
        # stupid simple and dirty timezone fix for CET
        hour = (hour + timezone_offset()) % 12
        #print(FORCE_UPDATE)
        if minute != m or FORCE_UPDATE:
            m = minute
            #d.show_time(hour,minute,col)
            d.show_time(hour,minute,DEFAULT_COLOR)
            #d.show_time(hour,minute)
            FORCE_UPDATE = False
            print("{} {} {}, {}:{}:{}".format(year,month,day,hour,minute,second))
        # on change of hour
        #if hour != h:
        # every 30 mins
        if not max(0,minute - 5) % 30 and second == 5:
            # h = hour
            set_time()
        if hour % 2 and minute == 15:
            try:
                connect_network()
                connect_mqtt()
            except OSError:
                pass
        yield

def main():
    global FORCE_NEW_COLOR
    # global c
    d = Display()
    DELAY = 100
    #global DEFAULT_COLOR
    timegen = show_clock(d)
    curr_gen = timegen
    OLD_COMMAND = ""
    # rgb_sparclegen = rgb_fixed_sparcle(d,numleds=NUMLEDS)
    # blue_sparclegen = blue_sparcle(d,numleds=NUMLEDS)
    # white_sparclegen = white_sparcle(d,numleds=NUMLEDS)
    # color_sparclegen = color_sparcle(d,numleds=NUMLEDS)
    while True:
        if CURR_COMMAND != OLD_COMMAND:
            init_new_command = True
        else:
            init_new_command = False

        if CURR_COMMAND == "clock" or CURR_COMMAND == "ON":
            if init_new_command:
                del curr_gen
                curr_gen = show_clock(d)
                OLD_COMMAND = CURR_COMMAND
            print(".",end="")
            #print(DEFAULT_COLOR)
            #next(timegen)
            next(curr_gen)
            utime.sleep_ms(DELAY)
        if CURR_COMMAND == "color_sparcle":
            DIMM = 0.7
            if init_new_command or FORCE_NEW_COLOR:
                FORCE_NEW_COLOR = False
                del curr_gen
                # curr_gen = rgb_fixed_sparcle(d,numleds=NUMLEDS)
                curr_gen = color_sparcle(d,MQTT_COLOR,numleds=NUMLEDS)
                OLD_COMMAND = CURR_COMMAND
            next(curr_gen)
        if CURR_COMMAND == "rgb_sparcle":
            DIMM = 0.7
            if init_new_command:
                del curr_gen
                curr_gen = rgb_fixed_sparcle(d,numleds=NUMLEDS)
                # curr_gen = color_sparcle(d,MQTT_COLOR,numleds=NUMLEDS)
                OLD_COMMAND = CURR_COMMAND
            next(curr_gen)
        if CURR_COMMAND == "blue_sparcle":
            if init_new_command:
                del curr_gen
                curr_gen = blue_sparcle(d,numleds=NUMLEDS)
                OLD_COMMAND = CURR_COMMAND
            next(curr_gen)
        if MQTT:
            c.check_msg()

if __name__ == "__main__":
    utime.sleep(5)
#    wlan = network.WLAN(network.STA_IF)
#    init_connect_network()
    connect_network()

    # gc.collect()
    # print("free ram")
    # print(gc.mem_free())

    set_time()
    connect_mqtt()
    utime.sleep(2)
    main()
