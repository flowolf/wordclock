# Wordclock


This is the source of the wordclocks I build. Both based on the WS2812 RGB
individually addressable LEDs. One driven by a Raspberry Pi, the other driven
by a ESP8266 board (nodemcu).

The code is far from production ready, but should give you a starting point.
It shows how I convert a classical time representation to a word-based time,
and how LEDs are mapped from a matrix view to the long array of LEDs.


The subfolder `esp` contains the code for the ESP clock, the subfolder `raspy`
contains the sources for the Raspberry Pi version.


## Talk

I held a talk about this project at [GLT18](https://gitlab.com/flowolf/glt18-talk)

